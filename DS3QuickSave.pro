#-------------------------------------------------
#
# Project created by QtCreator 2016-11-20T20:06:45
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DS3QuickSave
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    renamefile.cpp \
    jsoncpp.cpp \
    utilities.cpp \
    setpath.cpp

HEADERS  += mainwindow.h \
    renamefile.h \
    json/json-forwards.h \
    json/json.h \
    utilities.h \
    setpath.h

FORMS    += mainwindow.ui \
    renamefile.ui \
    setpath.ui

RESOURCES += \
    icons.qrc

DISTFILES += \
    icon.rc

RC_FILE = icon.rc
