#include "setpath.h"
#include "ui_setpath.h"

setPath::setPath(bool _customDir, QString _prePath, QWidget *parent) :
    QDialog(parent), ui(new Ui::setPath)
{
    ui->setupUi(this);
    this->customDir = _customDir;
    this->prePath = _prePath;

    QString     title;
    QString     ex;

    if (customDir)
    {
        title = "Set path to your personnal save folder";
        ex = "(ex: C:/Users/<USERNAME>/Documents/DS3_BackupSaves/)";
        this->ui->labelEx->setAlignment(Qt::AlignCenter);
    }
    else
    {
        title = "Set path to the Dark Souls III save folder";
        ex = "(ex: C:/Users/<USERNAME>/AppData/Roaming/DarkSoulsIII/01100001029c400d/)";
    }
    this->ui->labelTitle->setText(title);
    this->ui->labelEx->setText(ex);
}

setPath::~setPath()
{
    delete ui;
}

QString         setPath::getPath()
{
    return (this->ui->lineEdit->text());
}

void setPath::on_toolButton_clicked()
{
    QFileDialog         dialog(this);
    QString             path;
    QString             text;

    if (this->customDir)
        text = "Open your personnal save folder";
    else
        text = "Open DarkSoulsIII save directory";

    path = dialog.getExistingDirectory(this, text, this->prePath, QFileDialog::ShowDirsOnly | QFileDialog::ReadOnly);

    if (!path.isEmpty())
    {
        QString         finalPath = path + "/";
        this->ui->lineEdit->setText(finalPath);
    }
}

void setPath::on_buttonBox_accepted()
{
    if (this->ui->lineEdit->text().isEmpty())
        QMessageBox::warning(this, tr("Field empty"), "Incorrect path !");
    else
        this->accept();
}
