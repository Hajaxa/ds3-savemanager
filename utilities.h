#ifndef UTILITIES_H
#define UTILITIES_H

#include <QVector>
#include <string>
#include <fstream>
#include <iostream>
#include <json/json.h>
#include <json/json-forwards.h>


QVector<std::string>        readFile();
void                        writeFile(std::string pathCustom, std::string pathDS3);

#endif // UTILITIES_H
