#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>
#include <QFileInfo>
#include <QDir>

#define             NB_SOUNDS               11
#define             KEY                     116                 // 116 = F5 (0x74 in hex)
#define             REFRESH_TIMER           15000               // Refresh every 15s
#define             DS3_FILE_NAME           "DS30000.sl2"

HHOOK               hHook                   = NULL;
QTimer              *delayResizeTimer       = new QTimer();
QTimer              *keySavePressedTimer    = new QTimer();
QTimer              *autoSaveTimer          = new QTimer();
QTimer              *refreshTabTimer        = new QTimer();



/*
 *                  GET KEYBOARD INPUTS
 */

void                UpdateKeyState(BYTE *keystate, int keycode)
{
    keystate[keycode] = GetKeyState(keycode);
}

LRESULT             CALLBACK MyLowLevelKeyBoardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    // WPARAM is WM_KEYDOWN, WM_KEYUP, WM_SYSKEYDOWN, or WM_SYSKEYUP
    // LPARAM is the key information

    // Get the key information
    KBDLLHOOKSTRUCT cKey = *((KBDLLHOOKSTRUCT*)lParam);

    //wchar_t         buffer[5];

    // Get the keyboard state
    BYTE            keyboard_state[256];
    GetKeyboardState(keyboard_state);
    UpdateKeyState(keyboard_state, VK_SHIFT);
    UpdateKeyState(keyboard_state, VK_CAPITAL);
    UpdateKeyState(keyboard_state, VK_CONTROL);
    UpdateKeyState(keyboard_state, VK_MENU);

    // Get keyboard layout
    //HKL             keyboard_layout = GetKeyboardLayout(0);

    // Get the name
    //char            lpszName[0x100] = {0};

    DWORD           dwMsg = 1;
    dwMsg += cKey.scanCode << 16;
    dwMsg += cKey.flags << 24;

    //int     i = GetKeyNameText(dwMsg, (LPTSTR)lpszName, 255);

    // Try to convert the key info
    //int             result = ToUnicodeEx(cKey.vkCode, cKey.scanCode, keyboard_state, buffer, 4, 0, keyboard_layout);
    //buffer[4] = L'\0';

    // Print the output
    //if (wParam == WM_KEYDOWN)
        //qDebug() << "key: "  << cKey.vkCode << " " << QString::fromUtf16((ushort*)buffer) << " " << QString::fromUtf16((ushort*)lpszName);

    if (cKey.vkCode == KEY && wParam == WM_KEYUP)
        keySavePressedTimer->start(5);
    return CallNextHookEx(hHook, nCode, wParam, lParam);
}



/*
 *          CONSTRUCTOR & DESTRUCTOR
 */


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Initial set of ds3 save path
    QVector<std::string>         paths = readFile();
    if (!paths.isEmpty())
    {
        this->ui->directoryPathCustomLineEdit->setText(paths[0].c_str());
        this->ui->directoryPathDS3LineEdit->setText(paths[1].c_str());
        this->pathToCustomDir = paths[0].c_str();
        this->pathToDS3Dir = paths[1].c_str();
    }
    if (this->pathToCustomDir.isEmpty())
    {
        setPath                 pathDialog(true, "./", this);

        if (pathDialog.exec() == QDialog::Accepted)
        {
            this->pathToCustomDir = pathDialog.getPath();
            this->ui->directoryPathCustomLineEdit->setText(this->pathToCustomDir);
        }
    }
    if (this->pathToDS3Dir.isEmpty())
    {
        QSettings               settings(QSettings::IniFormat, QSettings::UserScope, "DarkSoulsIII");
        QString                 location = QFileInfo(settings.fileName()).absolutePath() + "/DarkSoulsIII";

        if (QDir(location).exists())
        {
            //this->pathToDS3Dir = location + "/";
            //this->defaultDarkSouls3Folder = this->pathToDS3Dir;
            this->defaultDarkSouls3Folder = location + "/";
        }
        else
            this->defaultDarkSouls3Folder = "./";

        setPath                 pathDialog(false, this->defaultDarkSouls3Folder, this);

        if (pathDialog.exec() == QDialog::Accepted)
        {
            this->pathToDS3Dir = pathDialog.getPath();
            this->ui->directoryPathDS3LineEdit->setText(this->pathToDS3Dir);
        }
    }

    // Initial set of keyboard inputs
    hHook = SetWindowsHookEx(WH_KEYBOARD_LL, MyLowLevelKeyBoardProc, NULL, 0);
    if (hHook == NULL)
        qDebug() << "Hook failed";
    else
    {
        keySavePressedTimer->setSingleShot(true);                                                // So it doesn't repeat every X ms
        connect(keySavePressedTimer, SIGNAL(timeout()), this, SLOT(on_buttonQuick_clicked()));
        connect(autoSaveTimer, SIGNAL(timeout()), this, SLOT(backupSave()));
    }

    // UI initial set up
    this->extendWindowSize();
    this->ui->groupBoxRestore->hide();
    this->loadSounds();

    // Utilities set up
    srand(time(NULL));
    delayResizeTimer->setSingleShot(true);
    connect(delayResizeTimer, SIGNAL(timeout()), this, SLOT(extendWindowSize()));
    connect(refreshTabTimer, SIGNAL(timeout()), this, SLOT(refreshTab()));
}

MainWindow::~MainWindow()
{
    if (!this->ui->directoryPathCustomLineEdit->text().isEmpty() && !this->ui->directoryPathDS3LineEdit->text().isEmpty())
        writeFile(this->ui->directoryPathCustomLineEdit->text().toStdString(), this->ui->directoryPathDS3LineEdit->text().toStdString());
    for (int i = 0; i < this->vecSounds.size(); ++i)
        if (this->vecSounds[i] != NULL)
            delete this->vecSounds[i];
    this->vecSounds.clear();

    autoSaveTimer->stop();
    refreshTabTimer->stop();

    delete keySavePressedTimer;
    delete autoSaveTimer;
    delete delayResizeTimer;
    delete refreshTabTimer;
    delete ui;
}


/*
 *          FUNCTIONS
 */

void                MainWindow::extendWindowSize()
{
    static bool     extend = false;
    QRect           newGeo = this->geometry();

    newGeo.setX(570);
    newGeo.setY(150);
    if (extend)
    {
        newGeo.setWidth(700);
        newGeo.setHeight(700);
    }
    else
    {
        newGeo.setWidth(700);
        newGeo.setHeight(300);
    }
    this->setGeometry(newGeo);
    extend = !extend;
}

void                MainWindow::backupSave()
{
    static int      nb_backup = 0;

    if (nb_backup == 5)
        nb_backup = 0;

    if (!this->pathToDS3Dir.isEmpty() && !this->pathToCustomDir.isEmpty())
    {
        QString     backupName = this->pathToCustomDir + DS3_FILE_NAME + ".backup" + QString::number(nb_backup);

        this->copyFile(this->pathToDS3Dir + DS3_FILE_NAME, backupName);
        ++nb_backup;
        this->playSound();
        this->initTables();
    }
    qDebug() << "Cannot save";
}

void                MainWindow::restoreSave(QString &fileName)
{
    // Copy and rename actual DS3 file to custom folder, then copy and rename selected file to DS3 folder

    if (!this->pathToDS3Dir.isEmpty() && !this->pathToCustomDir.isEmpty())
    {
        QString     src_ds3FileName = this->pathToDS3Dir + DS3_FILE_NAME;
        QString     dest_ds3FileName = this->pathToCustomDir + DS3_FILE_NAME + ".backupBeforeRestore";

        QString     src_customFileName = this->pathToCustomDir + fileName;
        QString     dest_customFileName = this->pathToDS3Dir + DS3_FILE_NAME;

        if (this->pathToCustomDir + fileName == dest_ds3FileName && QFile::exists(dest_ds3FileName))
        {
            QString backupOfBackup = dest_ds3FileName + "copy";
            this->copyFile(dest_ds3FileName, backupOfBackup);

            this->copyFile(src_ds3FileName, dest_ds3FileName);
            this->copyFile(backupOfBackup, dest_customFileName);

            QFile::remove(backupOfBackup);
        }
        else
        {
            this->copyFile(src_ds3FileName, dest_ds3FileName);
            this->copyFile(src_customFileName, dest_customFileName);
        }
        this->initTables();
    }
    else
        qDebug() << "Cannot restore, some paths are empty";
}

void                MainWindow::copyFile(QString srcFileName, QString destFileName)
{
    if (QFile::exists(destFileName))
        QFile::remove(destFileName);
    if (QFile::copy(srcFileName, destFileName))
        return ;
    else
        qDebug() << "File not copied";
}

void                MainWindow::renameFile(QString originalName)
{
    RenameFile      dialog(originalName, this);

    if (dialog.exec() == QDialog::Accepted)
    {
        QString     newPath = this->pathToCustomDir + dialog.getNewName();
        QString     ogPath = this->pathToCustomDir + originalName;
        QString     warningMessage = "File " + dialog.getNewName() + " already exists !";

        if (QFile::exists(newPath))
        {
            QMessageBox::warning(this, tr("File exists"), warningMessage);
            this->renameFile(originalName);
        }
        else
        {
            this->copyFile(ogPath, newPath);
            QFile::remove(ogPath);
            this->initTables();
        }
    }
}

void                MainWindow::refreshTab()
{
    this->initTables();
}

void                MainWindow::loadSounds()
{
    QString     soundpath   = "sounds/";
    QString     soundname   = "saved";
    QString     type        = ".wav";

    for (int i = 0; i < NB_SOUNDS; ++i)
    {
        QSound      *sound = new QSound(soundpath + soundname + QString::number(i + 1) + type, this);
        this->vecSounds.push_back(sound);
    }
}

void                MainWindow::playSound()
{
    int             indexSound;

    indexSound = rand() % 10;
    qDebug() << "Sound to be played: " << indexSound;
    if (indexSound < this->vecSounds.size())
        this->vecSounds[indexSound]->play();
    else
        qDebug() << indexSound << " is out of range of vector size: " << this->vecSounds.size();

}

void                MainWindow::setStoppedState()
{
    this->ui->actionStop->setEnabled(false);
    this->ui->actionStart->setEnabled(true);
    this->ui->actionRestore->setEnabled(true);

    this->ui->groupBoxLocateCustom->setEnabled(true);
    this->ui->groupBoxLocateDS3->setEnabled(true);
    this->ui->groupBoxAutoSave->setEnabled(true);
    this->ui->groupBoxQuickSave->setEnabled(true);

    this->ui->tableBackup->setEnabled(true);
    this->ui->tableDS->setEnabled(true);
    this->ui->buttonRight->setEnabled(true);
}

void                MainWindow::setActiveState()
{
    this->ui->actionStop->setEnabled(true);
    this->ui->actionStart->setEnabled(false);
    this->ui->actionRestore->setEnabled(false);

    this->ui->groupBoxLocateCustom->setEnabled(false);
    this->ui->groupBoxLocateDS3->setEnabled(false);
    this->ui->groupBoxAutoSave->setEnabled(false);
    this->ui->groupBoxQuickSave->setEnabled(false);

    this->ui->tableBackup->setEnabled(false);
    this->ui->tableDS->setEnabled(false);
    this->ui->buttonRight->setEnabled(false);
}

void                MainWindow::initTables()
{
    QDir    customDir(this->pathToCustomDir);
    QDir    ds3Dir(this->pathToDS3Dir);
    QStringList titles = {"Name", "Size", "Date"};

    this->ui->tableBackup->clear();
    this->ui->tableBackup->setRowCount(0);
    this->ui->tableBackup->setHorizontalHeaderLabels(titles);

    this->ui->tableDS->clear();
    this->ui->tableDS->setRowCount(0);
    this->ui->tableDS->setHorizontalHeaderLabels(titles);

    this->ui->buttonRight->setDisabled(true);

    if (checkDir(customDir))
        this->fillTable(customDir, this->ui->tableBackup);
    if (checkDir(ds3Dir))
        this->fillTable(ds3Dir, this->ui->tableDS);
}

bool                MainWindow::checkDir(QDir &dir)
{
    if (dir.exists())
        return (true);

    qDebug() << "directory doesn't exist";
    return (false);
}

void                MainWindow::fillTable(QDir &dir, QTableWidget *table)
{
    dir.setNameFilters(QStringList() << "*.sl2" << "*.backup*");
    QFileInfoList allFiles = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::Files, QDir::Name);

    QList<QFileInfo>::iterator i;
    for (i = allFiles.begin(); i != allFiles.end(); ++i)
    {
        QFileInfo   &info = *i;

        table->setRowCount(table->rowCount() + 1);
        table->setItem(table->rowCount() - 1, 0, new QTableWidgetItem(info.fileName()));
        table->setItem(table->rowCount() - 1, 1, new QTableWidgetItem(QString::number(info.size() / 1000) + " ko"));
        table->setItem(table->rowCount() - 1, 2, new QTableWidgetItem(info.lastModified().toString()));

        //qDebug() << "Path: " << info.absoluteFilePath(); // "C:/Users/VDPREDATOR/AppData/Roaming/DarkSoulsIII/DS30000.sl2"
        //qDebug() << "Name: " << info.fileName(); // "DS30000.sl2"
        //qDebug() << "Size: " << info.size(); // 9307472
        //qDebug() << "Date: " << info.lastModified().toString(); // "dim. nov. 20 18:13:04 2016"
    }
}



/*
 *      SLOTS
 */


void                MainWindow::on_buttonDS3SaveBrowser_clicked()
{
    QFileDialog         dialog(this);

    this->pathToDS3Dir = dialog.getExistingDirectory(this, tr("Open DarkSoulsIII save directory"), this->pathToDS3Dir,
                                                  QFileDialog::ShowDirsOnly | QFileDialog::ReadOnly);

    if (!this->pathToDS3Dir.isEmpty())
    {
        qDebug() << this->pathToDS3Dir;
        this->pathToDS3Dir = this->pathToDS3Dir + "/";
        this->ui->directoryPathDS3LineEdit->setText(this->pathToDS3Dir);
        this->initTables();
    }
}

void                MainWindow::on_buttonCustomSaveBrowser_clicked()
{
    QFileDialog     dialog(this);
    QString         nativeDir = "./";

    if (!this->pathToCustomDir.isEmpty())
        nativeDir = this->pathToCustomDir;
    this->pathToCustomDir = dialog.getExistingDirectory(this, tr("Open your personnal save directory"), nativeDir,
                                                  QFileDialog::ShowDirsOnly | QFileDialog::ReadOnly);

    if (!this->pathToCustomDir.isEmpty())
    {
        qDebug() << this->pathToCustomDir;
        this->pathToCustomDir = this->pathToCustomDir + "/";
        this->ui->directoryPathCustomLineEdit->setText(this->pathToCustomDir);
        this->initTables();
    }
}

void                MainWindow::on_buttonQuick_clicked()
{
    if (this->ui->groupBoxQuickSave->isChecked() && !this->ui->actionStart->isEnabled())
        this->backupSave();
}

void                MainWindow::on_actionStart_triggered()
{
    if (this->pathToCustomDir.isEmpty())
        QMessageBox::warning(this, tr("Field empty"), "You must locate your local save folder !");
    if (this->pathToDS3Dir == this->defaultDarkSouls3Folder)
        QMessageBox::warning(this, tr("Field empty"), "You must locate the Dark Souls III save folder !");
    if (this->pathToCustomDir.isEmpty() || this->pathToDS3Dir == this->defaultDarkSouls3Folder)
        return;
    this->setActiveState();
    if (this->ui->groupBoxAutoSave->isChecked())
        autoSaveTimer->start(this->ui->spinBoxAutoSave->value() * 1000);
}

void                MainWindow::on_actionStop_triggered()
{
    this->setStoppedState();
    if (autoSaveTimer->isActive())
        autoSaveTimer->stop();
}

void                MainWindow::on_actionRestore_triggered()
{
    static bool pressed = false;

    if (!pressed)
    {
        this->initTables();
        this->ui->groupBoxRestore->show();
        refreshTabTimer->start(REFRESH_TIMER);
    }
    if (pressed)
    {
        this->ui->groupBoxRestore->hide();
        refreshTabTimer->stop();
    }
    pressed = !pressed;
    delayResizeTimer->start(10);
}

void                MainWindow::on_tableBackup_cellClicked(int row, int column)
{
    row = row;
    column = column;
    this->ui->buttonRight->setEnabled(true);
}

void                MainWindow::on_tableBackup_cellDoubleClicked(int row, int column)
{
    this->renameFile(this->ui->tableBackup->item(row, column)->text());
}

void                MainWindow::on_buttonRight_clicked()
{
    QString         fileName = this->ui->tableBackup->item(this->ui->tableBackup->currentRow(), 0)->text();

    this->restoreSave(fileName);
}
