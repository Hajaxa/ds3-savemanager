#ifndef SETPATH_H
#define SETPATH_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>

namespace Ui {
class setPath;
}

class setPath : public QDialog
{
    Q_OBJECT

public:
    explicit setPath(bool _customDir, QString _prePath, QWidget *parent = 0);
    ~setPath();

    QString         getPath();

private slots:
    void            on_toolButton_clicked();
    void            on_buttonBox_accepted();

private:
    Ui::setPath     *ui;
    bool            customDir;
    QString         prePath;
};

#endif // SETPATH_H
