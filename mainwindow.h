#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardPaths>
#include <QTableWidget>
#include <QResizeEvent>
#include <QDateTime>
#include <QTimer>
#include <QDebug>
#include <QSound>
#include <QFile>

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <windows.h>

#include "utilities.h"
#include "setpath.h"
#include "renamefile.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void                setStoppedState();
    void                setActiveState();

    void                loadSounds();
    void                playSound();

    void                initTables();
    bool                checkDir(QDir &dir);
    void                fillTable(QDir &dir, QTableWidget *table);


private slots:
    void                extendWindowSize();
    void                backupSave();
    void                restoreSave(QString &fileName);
    void                copyFile(QString srcFileName, QString destFileName);
    void                renameFile(QString originalName);
    void                refreshTab();

    void                on_actionStart_triggered();
    void                on_actionStop_triggered();
    void                on_actionRestore_triggered();

    void                on_buttonQuick_clicked();
    void                on_buttonRight_clicked();
    void                on_buttonDS3SaveBrowser_clicked();
    void                on_buttonCustomSaveBrowser_clicked();

    void                on_tableBackup_cellClicked(int row, int column);
    void                on_tableBackup_cellDoubleClicked(int row, int column);

private:
    Ui::MainWindow      *ui;
    QString             defaultDarkSouls3Folder;
    QString             pathToDS3Dir;
    QString             pathToCustomDir;
    QVector<QSound*>    vecSounds;

};

#endif // MAINWINDOW_H
