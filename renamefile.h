#ifndef RENAMEFILE_H
#define RENAMEFILE_H

#include <QDialog>
#include <QMessageBox>

namespace Ui {
class RenameFile;
}

class RenameFile : public QDialog
{
    Q_OBJECT

public:
    explicit RenameFile(QString &originalName, QWidget *parent = 0);
    ~RenameFile();

    QString         getNewName();

private slots:
    void            on_buttonBox_accepted();

private:
    Ui::RenameFile *ui;
};

#endif // RENAMEFILE_H
