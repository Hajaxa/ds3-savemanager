#include "renamefile.h"
#include "ui_renamefile.h"

RenameFile::RenameFile(QString &originalName, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RenameFile)
{
    ui->setupUi(this);
    this->ui->newNameLineEdit->setText(originalName);
}

RenameFile::~RenameFile()
{
    delete ui;
}


QString         RenameFile::getNewName()
{
    return (this->ui->newNameLineEdit->text());
}

void            RenameFile::on_buttonBox_accepted()
{
    if (this->ui->newNameLineEdit->text().isEmpty())
        QMessageBox::warning(this, tr("Field empty"), "Please enter a file name");
    else
    {
        if (!this->ui->newNameLineEdit->text().contains(".sl2", Qt::CaseInsensitive))
            this->ui->newNameLineEdit->setText(this->ui->newNameLineEdit->text() + ".sl2");
        this->accept();
    }
}
