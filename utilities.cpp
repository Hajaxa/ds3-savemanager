#include "utilities.h"

QVector<std::string>    readFile()
{
    Json::Value         root;
    Json::Reader        reader;
    std::ifstream       file("pathPref.json", std::ifstream::binary);
    std::string         customPath;
    std::string         ds3Path;
    bool                parsingSuccessful = reader.parse(file, root);

    if (!parsingSuccessful)
    {
        std::cout << "Error: " << reader.getFormattedErrorMessages() << "\n";
        return (QVector<std::string>());
    }

    customPath = root.get("Custom path", 0).asString();
    ds3Path = root.get("DS3 path", 0).asString();
    return (QVector<std::string>() << customPath << ds3Path);
}

void                    writeFile(std::string pathCustom, std::string pathDS3)
{
    Json::Value         newEntry;
    std::string         jsonToStr;
    Json::StyledWriter  styledWriter;
    std::ofstream       file;

    newEntry["Custom path"] = pathCustom;
    newEntry["DS3 path"] = pathDS3;
    jsonToStr = styledWriter.write(newEntry);
    file.open("pathPref.json", std::ofstream::out | std::ofstream::trunc);
    if (!file.is_open())
        std::cout << "Couldn't open pathPref.json; function writeFile\n";
    else
    {
        file << jsonToStr << "\n";
        file.close();
    }
}
